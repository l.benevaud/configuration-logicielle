% Configuration logicielle
%
% Auteur : Grégory DAVID
%%

\documentclass[12pt,a4paper,oneside,titlepage,final]{exam}

\usepackage{style/layout}
\usepackage{style/glossaire}

\newcommand{\MONTITRE}{Gestion des configurations}
\newcommand{\MONSOUSTITRE}{Configuration logicielle}
\newcommand{\DISCIPLINE}{\glsentrytext{PPE} -- \glsentrydesc{PPE}}

\usepackage[%
	pdftex,%
	pdfpagelabels=true,%
	pdftitle={\MONTITRE},%
	pdfauthor={Grégory DAVID},%
	pdfsubject={\MONSOUSTITRE},%
  hidelinks,%
]{hyperref}
\usepackage{style/commands}

\title{%
  \begin{flushright}
      \noindent{\Huge {\bf \MONTITRE}} \\
      \noindent{\huge \MONSOUSTITRE} \\
      \noindent{\large \DISCIPLINE} \\
  \end{flushright}
}

%\printanswers\groolotPhiligranne{CORRECTION}

\begin{document}
% Page de titre
\maketitle

% Copyright
\include{copyright}

% Contenu
\HEADER{\MONTITRE}{\DISCIPLINE}

\section{Contexte}\label{sec:contexte}

Vous êtes en collaboration avec une équipe de développement logiciel
dont l'objectif est d'assurer une automatisation du pilotage d'une
webradio : \emph{radio6mic}.

Dans le cadre du développement logiciel, cette équipe repose sur un
environnement de développement, de mise au point, de construction et
d'exécution \gls{GNULINUX} \texttt{x86\_64} avec jeu d'instruction
exclusivement \texttt{amd64}. Les contraintes de fonctionnement sont
intimement liées à la plateforme Debian \gls{GNULINUX}. Ainsi,
l'équipe et son client principal n'escomptent pas exploiter une autre
plateforme logicielle pour des raisons majoritairement éthiques (libre
partage de la connaissance, du savoir et de l'usage). La seconde
raison principale étant la qualité de l'environnement de travail,
permettant de réaliser des projets au plus proche des besoins évoqués
par le client ou les équipes de travail.

Aujourd'hui, l'équipe de développement, pour qui le projet n'est pas
encore abouti, réalise le codage d'un programme de génération
automatique de \emph{playlist} selon critères utilisateur. Vous n'êtes
pas directement associé à l'équipe de développement ni à celle de
l'administration système. En revanche, vous êtes l'intermédiaire entre
les deux.

Cette position peut sembler inconfortable, mais elle repose sur vos
compétences à établir deux configurations logicielles robustes et
pérennes :
\begin{itemize}
  \item La première devra permettre au future programme d'être compilé
  dans un environnement cohérent et justement dimensionné. Nous
  nommerons la mission pour cette configuration logicielle
  \texttt{conf-construction}.

  \item La seconde sera celle qui assurrera au programme un
  environnement d'exécution adapté pour l'utilisateur final. Nous
  nommerons la mission pour cette configuration logicielle
  \texttt{conf-execution}.
\end{itemize}

\subsection{Environnement de
  développement}\label{sec:environnement.developpement}

L'équipe de développement travaille sur des environnements dont la
maîtrise leur est laissée. C'est à dire qu'ils sont capables
d'installer, supprimer, modifier et adapter leur environnement dans
les moindres détails. Cette liberté dont chaque membre de l'équipe de
développement dispose permet d'assurer une plus grande flexibilité
dans la recherche et la mise en œuvre de technologies diverses.

Par conséquent nous observons une très grande diversité des systèmes
et des configurations. Chaque développeur a sa propre configuration
finalement.

L'inconvénient c'est qu'il n'y a pas d'environnement consolidé qui
permette de dire : \og{}Ça fonctionne dans tous les cas~\fg.

En effet, pour l'instant, l'équipe de développement peut seulement
dire : \og{}Ça fonctionne sur ma machine !~\fg, et ça n'est absolument
pas satisfaisant.

Il est donc primordial de mettre en œuvre un environnement dont la
configuration logicielle est stable, cohérente, consolidée, documentée
et pérenne.

\subsection{Environnement de construction :
  \texttt{conf-construction}}\label{sec:environnement.construction}

Pour cela, un environnement de construction homogène, documenté, testé
et validé est indispensable pour assurer la qualité du projet et de sa
diffusion.

Cet environnement devra uniquement posséder les éléments de base du
système ainsi que les composants nécessaires à la construction du
programme (logiciels d'analyse, de compilation, de consruction, de
test, composants logiciels, bibliothèques logicielles).

L'objectif de cet environnement est uniquement d'assurer la
construction logicielle : passer du code source à l'exécutable.

En revanche, cet environnement n'est pas celui de l'utilisateur
final. En effet, l'utilisateur final du programme n'a pas besoin de
disposer des composants de construction logicielle ; il ne doit
posséder que le strict nécessaire à l'exécution du
programme. L'utilisateur final n'aura pas besoin de reconstruire le
programme, il n'en est que l'utilisateur.

La documentation qui sera produite pour décrire la configuration de
l'environnement de construction, devra servir à la
répétabilité\footnote{voir~\url{https://fr.wikipedia.org/wiki/R\%C3\%A9p\%C3\%A9tabilit\%C3\%A9}}
d'un environnement approprié à la construction logicielle.

\subsection{Environnement d'exécution :
  \texttt{conf-execution}}\label{sec:environnement.execution}

Ainsi, de la même façon que pour l'environnement de construction
(voir~\vref{sec:environnement.construction}), il est nécessaire de
définir, documenter, tester et valider l'environnement minimal pour
l'exécution du programme.

L'utilisateur n'aura que de la documentation à lire pour connaître les
éléments obligatoire à installer sur son environnement pour pouvoir
exécuter convenablement le programme. Sans cette documentation,
l'utilisateur pourra se retrouver face à un message d'erreur qui, en
fonction du niveau de compétences de cet utilisateur, sera peut-être
appréhendé de façon inappropriée.

Ici, en plus que de mettre en œuvre la configuration minimale, il faut
absolument considérer la documentation et son niveau d'accès, pour que
tous les utilisateurs sachent exactement ce qui doit être disponible
dans leur environnement système.

\subsection{Accès au code source}\label{sec:acces.code.source}

L'équipe de développement utilise la plateforme \gls{framagit} pour
héberger son projet. L'ensemble des collaborateurs peuvent ainsi
travailler sur le même code source en parallèle sans jamais perdre les
modifications réalisées.

Voir \url{https://framagit.org/sio/ppe1/genpl}.

Pour récupérer le code source au complet dans un répertoire, il suffit
de suivre la procédure suivante :
\begin{lstlisting}
git clone https://framagit.org/sio/ppe1/genpl.git
\end{lstlisting}


\clearpage%
\section{Travail à faire}\label{sec:travail.a.faire}
\begin{questions}
  \subsection{Préparation du projet de travail sur
    \gls{framagit}}\label{sec:preparation.projet.framagit}

  \question\label{q:framagit.create} Créer un projet nommé
  \texttt{p.nom-configuration-logicielle} à l'intérieur du groupe de
  travail \texttt{sio-malraux/sts1/ppe1}, comme dans l'exemple en
  \figurename~\vref{fig:framagit.create}.

  \begin{figure}[p!]
    \centering \fbox{
      \includegraphics[width=\textwidth]{data/framagit_create.png} }
    \caption{Création du projet}\label{fig:framagit.create}
  \end{figure}

  \question\label{q:framagit.set} Configurer le projet afin qu'il soit
  publique et qu'il ne propose que les fonctionnalités suivantes (voir
  l'exemple en \figurename~\vref{fig:framagit.set}) :
  \begin{itemize}
    \item \emph{Issues} (gestion des \glspl{bug}),
    \item \emph{Repository} (système de gestion de version \gls{git}),
    \item \emph{Merge Requests},
    \item \emph{\gls{wiki}}.
  \end{itemize}

  \begin{figure}[p!]
    \centering \fbox{
      \includegraphics[width=0.5\textwidth]{data/framagit_set.png}}
    \caption{Configuration du projet}\label{fig:framagit.set}
  \end{figure}

  \question\label{q:framagit.documentation} Chaque documentation à
  produire devra être une page \gls{wiki} correctement nommée, et
  toutes les pages \gls{wiki} devront apparaître sur la page
  \gls{wiki} \texttt{Home}.

  \question\label{q:framagit.repository} Chaque fichier modifié devra
  faire l'objet d'un correctif avec les outils \texttt{diff} et
  \texttt{patch}, puis stocké dans le \emph{Repository} \gls{git} via
  l'interface (exemple en
  \figurename~\vrefrange{fig:framagit.uploadfile}{fig:framagit.upload.commit}).

  \begin{figure}[p!]
    \centering \fbox{
      \includegraphics[width=\textwidth]{data/framagit_uploadfile.png}}
    \caption{Ajouter un fichier au \emph{Repository} depuis
      l'interface de \gls{gitlab}}\label{fig:framagit.uploadfile}
  \end{figure}
  \begin{figure}[p!]
    \centering \fbox{
      \includegraphics[width=\textwidth]{data/framagit_upload_commit.png}}
    \caption{Convenablement commenter la nature de l'ajout de
      fichier}\label{fig:framagit.upload.commit}
  \end{figure}


  \subsection{Préparation des systèmes de
    base}\label{sec:preparation.systemes.base}

  \question\label{q:install.debian.construction} Installer un système
  de base sur une machine dédiée à la construction, nommée
  \texttt{conf-construction}.

  \question\label{q:install.debian.excution} Installer un système de
  base sur une machine dédiée à l'exécution, nommée
  \texttt{conf-execution}.

  \subsection{Construction}\label{sec:construction}
  \subsubsection{Identification des problèmes de
    construction}\label{sec:identification.problemes.construction}

  \question\label{q:construire.avec.autotools} Réaliser la
  construction logicielle suivant les principes des \texttt{autotools}
  :
  \begin{enumerate}
    \item \texttt{autoreconf -{}-install}
    \item \texttt{./configure}
    \item \texttt{make}
    \item \texttt{make check}
    \item \texttt{make install} (en tant qu'utilisateur privilégié)
  \end{enumerate}

  \question\label{q:identifier.dysfonctionnements} En déduire et
  identifier le ou les dysfonctionnements. Noter qu'il est possible
  que tous les dysfonctionnements n'apparaissent pas du premier coup.

  \question\label{q:recenser.composants} Recenser l'ensemble des
  composants logiciels dont le projet dépend.

  \subsubsection{Correction des dysfonctionnements de
    construction}\label{sec:correction.dysfonctionnements.construction}

  \question\label{q:} Corriger l'ensemble des dysfonctionnements dans
  les fichiers \texttt{configure.ac}, \texttt{src/Makefile.am} et
  \texttt{tests/Makefile.am} (voir les \lstlistingname
  s~\vrefrange{lst:configure.ac}{lst:tests.Makefile.am}).

  \question\label{q:rediger.rapport.test.validation} Rédiger le
  rapport de test de validation (reposez-vous sur les sorties de
  \texttt{./configure}, \texttt{make} et \texttt{make check}).

  \question\label{q:rediger.documentation.construction} Rédiger la
  documentation des prérequis minimaux pour assurer la construction
  logicielle.

  \subsection{Exécution}\label{sec:execution}
  \subsubsection{Identification des problèmes
    d'exécution}\label{sec:identification.problemes.execution}

  \question\label{q:installer.executable} Copier le programme
  exécutable, construit depuis \texttt{conf-construction}, dans le
  répertoire commun des programmes dans \texttt{conf-execution}. Avant
  de l'exécuter, vérifier que l'ensemble des bibliothèques logicielles
  dont il dépend sont disponibles sur le système.

  \subsubsection{Correction des dysfonctionnements
    d'exécution}\label{sec:correction.dysfonctionnements.execution}

  \question\label{q:installer.dependances.minimales} Installer sur
  l'environnement d'exécution les composants minimaux nécessaires à la
  bonne marche du programme.

  \question\label{q:rediger.documentation.execution} Rédiger la
  documentation des prérequis minimaux pour assurer l'exécution du
  programme dans un environnement quelconque.
\end{questions}


\clearpage%
\section{Ressources}\label{sec:ressources}

\subsection{Installation d'un système Debian
  \gls{GNULINUX}}\label{sec:installation.debian}

\paragraph{Système de base}
\begin{itemize}
  \item \url{https://www.debian.org/releases/stable/amd64/}
  \item \url{https://www.debian.org/distrib/netinst}
\end{itemize}

\paragraph{Plus spécifiquement pour la construction}
\begin{itemize}
  \item \url{https://www.gnu.org/prep/standards/standards.html#Configuration}
  \item \url{https://packages.debian.org/buster/build-essential}
  \item \url{https://packages.debian.org/buster/autoconf}
  \item \url{https://packages.debian.org/buster/automake}
  \item \url{https://packages.debian.org/buster/libtool}
\end{itemize}

\subsection{Autotools}\label{sec:autotools}
\begin{itemize}
  \item
  \url{https://www.gnu.org/savannah-checkouts/gnu/autoconf/manual/autoconf-2.69/autoconf.html}
  \item
  \url{https://www.gnu.org/software/automake/manual/automake.html}
  \item \url{https://autotools.io/index.html}
\end{itemize}

\subsection{Utilitaire de gestion des drapeaux de compilation
  \texttt{pkg-config}}\label{sec:utilitaire.pkg-config}
\texttt{pkg-config} est un logiciel qui fournit une interface unifiée
pour interroger les bibliothèques installées lors de la compilation de
code source qui utilise une de ces bibliothèques.

La paquet \texttt{pkg-config} fournit un système de gestion des
drapeaux de compilation et d'édition de lien qui fonctionne avec
\texttt{automake} et \texttt{autoconf}.

De nombreuses bibliothèques logicielles sont fournies avec des
fichiers \og{}.pc~\fg{} qui permettent de retrouver les drapeaux de
compilation et d'édition de lien nécessaires et de les utiliser avec
le programme \texttt{pkg-config}.

Par ailleurs \texttt{pkg-config} fourni un ensemble de macros pour
\texttt{autoconf} afin de faciliter son intégration dans les fichiers
\texttt{configure.ac}.

Voir :
\begin{itemize}
  \item \url{https://packages.debian.org/buster/pkg-config}
  \item \texttt{man pkg-config}, et plus particuliairement la section
  \texttt{AUTOCONF MACROS} avec les macros \texttt{PKG\_PREREQ()} et
  \texttt{PKG\_CHECK\_MODULES()}
\end{itemize}

\subsection{Fichiers à corriger pour la
  construction}\label{sec:fichiers.a.corriger.construction}

Les \lstlistingname
s~\vrefrange{lst:configure.ac}{lst:tests.Makefile.am} présentent la
configuration de base du projet avec les dysfonctionnements.

\lstset{language=[gnu]make, basicstyle={\ttfamily\scriptsize}}%
\lstinputlisting[caption={\lstname},
label={lst:configure.ac}]{genpl/configure.ac}

\lstinputlisting[caption={\lstname},
label={lst:src.Makefile.am}]{genpl/src/Makefile.am}

\lstinputlisting[caption={\lstname},
label={lst:tests.Makefile.am}]{genpl/tests/Makefile.am}


\clearpage
\section{Couverture du référentiel de certification
  \gls{SIO}}\label{sec:couverture.referentiel.sio}

Le contenu de ce document permet d'aborder ou de couvrir les activités
et compétences suivantes :
\begin{enumerate}
  \item [\textbf{A1.1.2}] Étude de l'impact de l'intégration d'un
  service sur le système informatique
  \begin{itemize}
    \item [\textbf{C1.1.2.1}] Analyser les interactions entre services
    \item [\textbf{C1.1.2.2}] Recenser les composants de
    l'architecture technique sur lesquels le service à produire aura
    un impact
  \end{itemize}
  \item [\textbf{A1.2.4}] Détermination des tests nécessaires à la
  validation d'un service
  \begin{itemize}
    \item [\textbf{C1.2.4.1}] Recenser les tests d'acceptation
    nécessaires à la validation du service et les résultats attendus
    \item [\textbf{C1.2.4.2}] Préparer les jeux d'essai et les
    procédures pour la réalisation des tests
  \end{itemize}
  \item [\textbf{A1.3.1}] Test d'intégration et d'acceptation d'un
  service
  \begin{itemize}
    \item [\textbf{C1.3.1.2}] Tester le service
    \item [\textbf{C1.3.1.3}] Rédiger le rapport de test
  \end{itemize}
  \item [\textbf{A2.3.1}] Identification, qualification et évaluation
  d'un problème
  \begin{itemize}
    \item [\textbf{C2.3.1.3}] Qualifier le problème (contexte et
    environnement)
    \item [\textbf{C2.3.1.5}] Évaluer les conséquences techniques du
    problème
  \end{itemize}
  \item [\textbf{A4.1.6}] Gestion d'environnements de développement et
  de test
  \item [\textbf{A4.1.9}] Rédaction d'une documentation technique
  \begin{itemize}
    \item [\textbf{C4.1.9.1}] Produire ou mettre à jour la
    documentation technique d'une solution applicative et de ses
    composants logiciels
  \end{itemize}
  \item [\textbf{A4.2.1}] Analyse et correction d'un
  dysfonctionnement, d'un problème de qualité de service ou de
  sécurité
  \begin{itemize}
    \item [\textbf{C4.2.1.3}] Concevoir les mises à jour à effectuer
    \item [\textbf{C4.2.1.4}] Réaliser les mises à jour
  \end{itemize}
  \item [\textbf{A4.2.2}] Adaptation d'une solution applicative aux
  évolutions de ses composants
  \begin{itemize}
    \item [\textbf{C4.2.2.1}] Repérer les évolutions des composants
    utilisés et leurs conséquences
    \item [\textbf{C4.2.2.2}] Concevoir les mises à jour à effectuer
  \end{itemize}
  \item [\textbf{A5.1.3}] Suivi d'une configuration et de ses éléments
  \begin{itemize}
    \item [\textbf{C5.1.3.1}] Contrôler et auditer les éléments de la
    configuration
    \item [\textbf{C5.1.3.2}] Reconstituer un historique des
    modifications effectuées sur les éléments de la configuration
    \item [\textbf{C5.1.3.3}] Identifier les éléments de la
    configuration à modifier ou à remplacer
    \item [\textbf{C5.1.3.4}] Repérer les équipements obsolètes et en
    proposer le traitement dans le respect de la réglementation en
    vigueur
  \end{itemize}
  \item [\textbf{A5.2.1}] Exploitation des référentiels, normes et
  standards adoptés par le prestataire informatique
  \begin{itemize}
    \item [\textbf{C5.2.1.1}] Évaluer le degré de conformité des
    pratiques à un référentiel, à une norme ou à un standard adopté
    par le prestataire informatique
    \item [\textbf{C5.2.1.2}] Identifier et partager les bonnes
    pratiques à intégrer
  \end{itemize}
\end{enumerate}

\clearpage%
% References bibliographiques
\printbibheading%
\printbibliography[nottype=online,check=notonline,heading=subbibliography,title={Bibliographiques}]
\printbibliography[check=online,heading=subbibliography,title={Webographiques}]

\printglossaries%

\end{document}
